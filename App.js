/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  Image,
  Pressable,
} from 'react-native';
import DocumentPicker, {isInProgress} from 'react-native-document-picker';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Dimensions} from 'react-native';
import FileViewer from 'react-native-file-viewer';

const awsS3Url = '';

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [result, setResult] = React.useState();
  const [url, setUrl] = React.useState();
  const [type, setType] = React.useState();
  const [html, setHtml] = React.useState();
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };
  useEffect(() => {
    console.log(JSON.stringify(result, null, 2));
  }, [result]);
  useEffect(() => {
    setHtml([createHtml()]);
  }, [url]);
  const handleError = err => {
    if (DocumentPicker.isCancel(err)) {
      console.warn('cancelled');
      // User cancelled the picker, exit any dialogs or menus and move on
    } else if (isInProgress(err)) {
      console.warn(
        'multiple pickers were opened, only the last will be considered',
      );
    } else {
      throw err;
    }
  };

  const createHtml = () => {
    if (url == undefined) return;
    console.log(url);
    const link = url[0];
    console.log(imageMessage(link));
    switch (type[0].split('/')[0]) {
      case 'image':
        return imageMessage(link);
      case 'audio':
        return audioMessage(link);
      case 'video':
        return videoMessage(link);
      default:
        return fileMessage(link);
    }
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          <Text
            style={{
              marginTop: 30,
              fontSize: 24,
              fontWeight: '600',
              textAlign: 'center',
            }}>
            Hola por favor suba un archivo
          </Text>
          <Pressable
            style={styles.button}
            onPress={async () => {
              try {
                const pickerResult = await DocumentPicker.pickSingle({
                  presentationStyle: 'fullScreen',
                  copyTo: 'cachesDirectory',
                });
                //await FileViewer.open(pickerResult.uri);
                setType([pickerResult.type]);
                setResult([pickerResult]);
              } catch (e) {
                handleError(e);
              }
            }}>
            <Text style={styles.text}>Abrir selector de archivos</Text>
          </Pressable>
          <Pressable
            style={styles.button}
            onPress={async () => {
              try {
                if (result != null) {
                  //If file selected then create FormData
                  // const fileToUpload = result;
                  // const data = new FormData();
                  // data.append('name', 'Image Upload');
                  // data.append('file_attachment', fileToUpload);
                  // let res = await fetch('URL_TO_SEND', {
                  //   method: 'post',
                  //   body: data,
                  //   headers: {
                  //     'Content-Type': 'multipart/form-data; ',
                  //   },
                  // });
                  // let responseJson = await res.json();
                  // if (responseJson.status == 1) {
                  //   alert('Upload Successful');
                  // }
                  setUrl([
                    'https://www.purina-latam.com/sites/g/files/auxxlc391/files/styles/social_share_large/public/Purina®%20Como%20elegir%20un%20nuevo%20gatito.jpg?itok=WOC5m4KQ',
                  ]);
                } else {
                  //if no file selected the show alert
                  alert('Please Select File first');
                }
              } catch (error) {
                console.log(error);
                alert('Error');
              }
            }}>
            <Text style={styles.text}>Enviar archivo</Text>
          </Pressable>
          <Text>{html == null ? null : html}</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

function fileDownloadLink(link) {
  return `
    <div style="position: absolute; bottom: 2px; right: 0px">
      <a
        href="${link}"
        target="_blank"
        download="true"
        style="
          background-color: rgba(0, 0, 0, 0.7);
          padding: 3px 10px;
          text-decoration: none;
          border-radius: 10px;
          color: white;
        "
        ><small>Ver archivo</small></a
      >
    </div>
  `;
}
function imageMessage(path) {
  return `
  <img
  src="${path}"
  alt=""
  style="width: 150px; border-radius: 5px; object-fit: cover" />
  ${fileDownloadLink(path)}
  `;
}
function videoMessage(path) {
  return `
    <video src="${path}" style="width: 200px; border-radius: 5px" controls></video>
    ${fileDownloadLink(path)}
  `;
}
function audioMessage(path) {
  return `
    <audio src="${path}" style="width: 216px;" controls></audio>
    ${fileDownloadLink(path)}
  `;
}
function fileMessage(path) {
  `
  <div style="width: 120px; padding: 15px 0px 34px; margin: 0 auto">
    <div style="width: 30px; height: 30px; margin: 0 auto">
      <img
        src="https://w7.pngwing.com/pngs/23/42/png-transparent-computer-icons-doc-others-thumbnail.png"
        style="object-fit: cover; width: 100%; height: 100%" />
    </div>
  </div>
  ${fileDownloadLink(path)}
  `;
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  button: {
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'black',
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
  image: {
    paddingTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width,
  },
});

export default App;
